
import time
import os
import msvcrt
import random

N = 20
M = 40

EMPTY  = ' '
FOOD   = chr(3)

H_WALL = '═'
V_WALL = '║'

WALL_1 = '╔'
WALL_2 = '╚'
WALL_3 = '╗'
WALL_4 = '╝'

SNAKE_H_BODY = '■'
SNAKE_V_BODY = '■'

SNAKE_HEAD_R = chr(16)
SNAKE_HEAD_L = chr(17)
SNAKE_HEAD_U = chr(30)
SNAKE_HEAD_D = chr(31)

class Coord:
    def __init__(self, x, y):
        self.x = x
        self.y = y

def getd(a, b):
    if a.y + 1 == b.y: return 'right'
    if a.y - 1 == b.y: return 'left'
    if a.x + 1 == b.x: return 'down'
    if a.x - 1 == b.x: return 'up'

    return None

class Snake:
    def __init__(self, x, l):
        self.coords = []
        for i in range(l, 0, -1):
            self.coords.append(Coord(x, i))
        self.d = 'right'
        self.genfood()

    def gets(self, x, y):
        p = self.coords
        d = None
        is_head = False

        for i in range(len(p)):
            if p[i].x == x and p[i].y == y:
                d = self.d if i == 0 else getd(p[i], p[i-1])
                is_head = i == 0
                break

        if d == None: return

        if is_head:
            if d == 'right': return SNAKE_HEAD_R
            elif d == 'left': return SNAKE_HEAD_L
            elif d == 'up': return SNAKE_HEAD_U
            elif d == 'down': return SNAKE_HEAD_D

        if d == 'right' or d == 'left': return SNAKE_H_BODY
        return SNAKE_V_BODY

    def genfood(self):
        while True:
            self.foodx = random.randint(1, N-2)
            self.foody = random.randint(1, M-2)

            isuniq = True

            for c in self.coords:
                if self.foodx == c.x and self.foody == c.y:
                    isuniq = False
                    break

            if isuniq: break

    def setd(self, d):
        if (d == 'up' and self.d != 'down') \
            or (d == 'down' and self.d != 'up') \
            or (d == 'left' and self.d != 'right') \
            or (d == 'right' and self.d != 'left'): self.d = d

    def move(self):
        dx = 0
        dy = 0

        if self.d == 'right':
            dx = 0
            dy = 1
        elif self.d == 'left':
            dx = 0
            dy = -1
        elif self.d == 'up':
            dx = -1
            dy = 0
        elif self.d == 'down':
            dx = 1
            dy = 0

        p = self.coords

        for i in range(len(p)-1, 0, -1):
            p[i].x = p[i-1].x
            p[i].y = p[i-1].y

        p[0].x += dx
        p[0].y += dy

        if p[0].x >= N-1: p[0].x = 1
        elif p[0].x <= 0: p[0].x = N-2

        if p[0].y >= M-1: p[0].y = 1
        elif p[0].y <= 0: p[0].y = M-2

        for i in range(1, len(p)):
            if p[0].x == p[i].x and p[0].y == p[i].y:
                raise Exception

        if p[0].x == self.foodx and p[0].y == self.foody:
            d = getd(p[len(p)-1], p[len(p)-2])

            if d == 'right': p.append(Coord(p[len(p)-1].x, p[len(p)-1].y - 1))
            elif d == 'left': p.append(Coord(p[len(p)-1].x, p[len(p)-1].y + 1))
            elif d == 'up': p.append(Coord(p[len(p)-1].x + 1, p[len(p)-1].y))
            elif d == 'down': p.append(Coord(p[len(p)-1].x - 1, p[len(p)-1].y))

            self.genfood()

def draw():
    os.system('cls')
    output = ''
    for i in range(N):
        for j in range(M):
            s = None
            if i == 0:
                if j == 0: s = WALL_1
                elif j == M-1: s = WALL_3
                else: s = H_WALL
            elif i == N-1:
                if j == 0: s = WALL_2
                elif j == M-1: s = WALL_4
                else: s = H_WALL
            else:
                if j == 0 or j == M-1: s = V_WALL
                elif i == snake.foodx and j == snake.foody: s = FOOD
                else:
                    s = snake.gets(i, j)
                    if s == None: s = EMPTY
            output += s
        output += '\n'

    output += '\nSnake length: ' + str(len(snake.coords))
    output += '\nPlease press ESC to exit '

    print(output)
    print()

    time.sleep(0.03)

if __name__ == '__main__':
    snake = Snake(3, 5)
    
    os.system('color B')

    while True:
        if msvcrt.kbhit():
            key = ord(msvcrt.getch())

            if key == 27: break
            elif key == 224: key = ord(msvcrt.getch())

            if key == 72 or key == 119:
                snake.setd('up')
            elif key == 80 or key == 115:
                snake.setd('down')
            elif key == 77 or key == 100:
                snake.setd('right')
            elif key == 75 or key == 97:
                snake.setd('left')

        try:
            snake.move()
        except:
            draw()
            break
        draw()

    print('written by: zh.adlet@gmail.com')
    input('Press Enter to exit')
